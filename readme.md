# Humane-Like Themes for CudaText v0.5

![Humane-Like.png](./Humane-Like.png?raw=true "Screenshot")

## About

UI and Syntax themes for CudaText editor

### Distribution

The .zip contains only the two theme files and the .inf file.

### History

0.5:

- Fix XML FG colors

0.4:

- Fix FG in statusbar panels during hover

0.3:

- Fix colors for comments, string literals, numeric lterals, Type names

0.2:

- Fix Find/Replace button colors

0.1:

- Initial release
